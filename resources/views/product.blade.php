@extends('layouts.app')

@section('content')

    <div class="modal " id="product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавление статьи</h5>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="{{asset('images/1.png')}}" class="images img-thumbnail" alt="...">
                    <div class="card-body">
                        <h3 class="card-title">Собака</h3>
                        <h5 class="card-text">185 грн</h5>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#buy">Купить</button>
                        <br>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
            <div class="col-md-5">
                <img src="{{asset('images/1.png')}}" class="img-thumbnail" alt="картинка">
            </div>
            <div class="col-md-7">
                <div class="card-body">
                    <h5 class="card-title">{{$product->name}}</h5>
                    <p class="card-text">{{$product->description}}</p>
                    <p class="card-text"><b>{{$product->price}} грн</b></p>
                </div>
            </div>
        </div>
    </div>

@endsection
