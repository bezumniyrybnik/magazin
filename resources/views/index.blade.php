@extends('layouts.app')

@section('content')

    <div class="modal" id="newProduct">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавление товара</h5>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('store')}}" enctype="multipart/form-data">
                        <input type="text" name="name" placeholder="Название" class="form-control mb-2">
                        <input type="text" name="description" placeholder="Описание"
                               class="form-control mb-2">
                        <select name="category_id" class="form-control mb-2" size="5">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <input type="text" name="price" placeholder="Цена" class="form-control mb-2">
                        <input type="file" name="image" class="mb-2"><br>
                        {{csrf_field()}}
                        <button>
                            <input type="submit" class="btn btn-default" value="Добавить">
                        </button>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <div class="products">
        <div class="row row-cols-1 row-cols-md-3">
            @foreach($products as $product)
                <div class="col mb-4">
                    <div class="card h-100">
                        <div class="image">
                            <img src="{{ $product->getImageUrl() }}" class="img-thumbnail" alt="картинка">
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">{{$product->name}}</h3>
                            <h5 class="card-text">{{$product->price}} грн</h5>
                            <a href="{{$product->link()}}">Обзор</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="btn-group" role="group" aria-label="First group">
        {{ $products->links() }}
    </div>

@endsection
