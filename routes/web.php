<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/product/{product}', [
    'as' => 'show',
    'uses' => 'ProductController@show',
]);

Route::post('/store', [
    'as' => 'store',
    'uses' => 'ProductController@store'
]);

Route::get('/{slug?}', [
    'as' => 'index',
    'uses' => 'ProductController@index',
]);
