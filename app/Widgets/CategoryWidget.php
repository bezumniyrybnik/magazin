<?php

namespace App\Widgets;

use App\Widgets\Contract\ContractWidget;
use App\Category;

class CategoryWidget implements ContractWidget
{
    public function execute()
    {
        $data = Category::all();
        return view('Widgets::category', [
            'data' => $data,
        ]);
    }
}
