@if($data)

    <nav class="">
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="#">Главная</a>
            </li>
            @foreach($data as $category)
                <li class="nav-item">
                    <a class="nav-link" href="{{$category->slug}}">
                        {{$category->name}}</a>
                </li>
            @endforeach
        </ul>
    </nav>

@endif
